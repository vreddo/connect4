﻿using UnityEngine;
using Zenject;

namespace VReddo.Connect4
{
    public class PhotonService : Photon.PunBehaviour, IPunTurnManagerCallbacks
    {
        #region Private Variables
        private GameManager gameManager;
        private string _gameVersion = "1";
        private bool isConnecting = false;
        private bool isMasterClient = false;
        #endregion

        #region Public Variables
        public byte maxPlayersPerRoom = 2;
        public PunTurnManager punTurnManager;
        public bool GetIsMasterClient
        {
            get { return isMasterClient; }
        }
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            // #Critical
            // we don't join the lobby. There is no need to join a lobby to get the list of rooms.
            PhotonNetwork.autoJoinLobby = false;

            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.automaticallySyncScene = true;
        }

        void Start()
        {
            punTurnManager = this.gameObject.AddComponent<PunTurnManager>();
            punTurnManager.TurnManagerListener = this;
            punTurnManager.TurnDuration = 0;
        }
        #endregion


        #region Public Methods
        [Inject]
        public void Constructor(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        public void OnConnect()
        {
            isConnecting = true;
            gameManager.connectBtn.gameObject.SetActive(false);
            gameManager.connectingTxt.SetActive(true);

            // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
            if (PhotonNetwork.connected)
            {
                // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                Debug.Log("ConnectUsingSettings");
                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.ConnectUsingSettings(_gameVersion);
            }
        }

        public void OnQuitGame()
        {
            PhotonNetwork.Disconnect();
        }
        #endregion

        #region Photon Overrides
        public override void OnConnectedToMaster()
        {
            Debug.Log("Region:" + PhotonNetwork.networkingPeer.CloudRegion);
            if (isConnecting)
            {
                Debug.Log("DemoAnimator/Launcher: OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room.\n Calling: PhotonNetwork.JoinRandomRoom(); Operation will fail if no room found");

                PhotonNetwork.JoinRandomRoom();
            }
        }

        public override void OnJoinedLobby()
        {
            OnConnectedToMaster(); // this way, it does not matter if we join a lobby or not
        }

        public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
        {
            Debug.Log("DemoAnimator/Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
            PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = maxPlayersPerRoom, PlayerTtl = 100 }, null);
            //PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = this.maxPlayersPerRoom }, null);
        }

        public override void OnJoinedRoom()
        {
            isConnecting = false;
            isMasterClient = PhotonNetwork.isMasterClient;
            gameManager.connectPanel.SetActive(false);
            gameManager.gameState = GameManager.GameState.CONNECTED;

            if (PhotonNetwork.room.PlayerCount == 2)
            {
                if (punTurnManager.Turn == 0)
                {
                    // when the room has two players, start the first turn (later on, joining players won't trigger a turn)
                    gameManager.StartTurn();
                    gameManager.gameState = GameManager.GameState.GAME_STARTED;
                }
            }
            else
            {
                gameManager.playerPrompt.text = "WAITING FOR OTHER PLAYER";
                Debug.Log("Waiting for another player");
            }

            //if (PhotonNetwork.room.PlayerCount == 1)
            //{
            //    Debug.Log("We load the 'Room for 1' ");
            //    //SceneManager.LoadScene("GameScene");
            //    //PhotonNetwork.LoadLevel("GameScene");
            //}
        }

        public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            Debug.Log("Other player arrived");

            if (PhotonNetwork.room.PlayerCount == 2)
            {
                if (this.punTurnManager.Turn == 0)
                {
                    // when the room has two players, start the first turn (later on, joining players won't trigger a turn)
                    gameManager.StartTurn();
                    gameManager.gameState = GameManager.GameState.GAME_STARTED;
                }
            }
        }

        public override void OnPhotonPlayerDisconnected(PhotonPlayer remotePlayer)
        {
            Debug.Log("OnPhotonPlayerDisconnected");
            gameManager.OnQuitGame();
        }

        public override void OnDisconnectedFromPhoton()
        {
            Debug.LogError("DemoAnimator/Launcher:Disconnected");

            // #Critical: we failed to connect or got disconnected. There is not much we can do. Typically, a UI system should be in place to let the user attemp to connect again.
            //loaderAnime.StopLoaderAnimation();

            //isConnecting = false;
            //controlPanel.SetActive(true);
        }
        #endregion

        #region TurnManager Callbacks
        public void OnTurnBegins(int turn)
        {
            //Debug.Log("OnTurnBegins() turn: " + turn);
        }

        public void OnTurnCompleted(int turn)
        {
            Debug.Log("OnTurnCompleted: " + turn);
            gameManager.StartTurn();
        }

        public void OnPlayerMove(PhotonPlayer player, int turn, object move)
        {
            //Debug.Log("OnPlayerMove: " + player + " turn: " + turn + " action: " + move);
        }

        public void OnPlayerFinished(PhotonPlayer player, int turn, object move)
        {
            //Debug.Log("OnTurnFinished: " + player + " turn: " + turn + " action: " + move);
            if (gameManager.gameState == GameManager.GameState.GAME_ENDED)
                return;

            if (!player.IsLocal)
            {
                gameManager.PlayerTurn = true;
                gameManager.playerPrompt.text = "YOUR TURN";
            }
            else
            {
                gameManager.PlayerTurn = false;
                gameManager.playerPrompt.text = "OTHER PLAYER TURN";
            }
        }

        public void OnTurnTimeEnds(int turn)
        {
            //Debug.Log("OnTurnTimeEnds: Calling OnTurnCompleted");
        }
        #endregion
    }
}

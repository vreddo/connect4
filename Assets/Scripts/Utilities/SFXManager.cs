﻿using UnityEngine;

namespace VReddo.Connect4
{
    public enum SFXClips
    {
        BUTTON_CLICK,
        SELECT_SLOT
    }

    public class SFXManager : MonoBehaviour
    {
        public AudioClip[] sfxClip;
        [SerializeField] private AudioSource[] audioSource;

        #region Public Methods
        public void PlayClip(SFXClips clip)
        {
            switch (clip)
            {
                case SFXClips.BUTTON_CLICK:
                    audioSource[0].clip = sfxClip[0];
                    audioSource[0].Play();
                    break;
                case SFXClips.SELECT_SLOT:
                    audioSource[0].clip = sfxClip[1];
                    audioSource[0].Play();
                    break;
            }
        }

        public void StopClip(SFXClips clip)
        {
            switch (clip)
            {
                case SFXClips.BUTTON_CLICK:
                    //audioSource[2].Stop();
                    break;
            }
        }
        #endregion
    }
}

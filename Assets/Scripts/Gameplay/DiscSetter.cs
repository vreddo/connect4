﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VReddo.Connect4
{
    public class DiscSetter : MonoBehaviour
    {
        #region Private Variables
        private MeshRenderer meshRenderer;
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            meshRenderer = this.GetComponent<MeshRenderer>();
        }
        #endregion

        #region Public Methods (Gameplay)
        public void SetDiscColor(string color)
        {
            meshRenderer.enabled = true;

            if (color.Contains("Red") || color.Contains("red"))
                meshRenderer.material.color = Color.red;
            else
                meshRenderer.material.color = Color.green;
        }

        public void ResetDisc()
        {
            meshRenderer.enabled = false;
        }
        #endregion
    }
}


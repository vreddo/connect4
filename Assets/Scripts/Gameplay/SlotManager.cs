﻿using UnityEngine;
using Zenject;

namespace VReddo.Connect4
{
    public class SlotManager : Photon.PunBehaviour
    {
        private GameManager gameManager;
        private PhotonService photonService;
        private SFXManager sfxManager;
        private int columns = 7;
        private int rows = 6;
        private SlotScript[,] slot = new SlotScript[7, 6]; // Temp Holder

        public SlotScript[] slotArr; // Inspector Objects

        #region Monobehaviour Methods
        void Start()
        {
            // Set slots to a multidimensional array
            int _iterator = 0;
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    slot[i, j] = slotArr[_iterator];
                    _iterator++;
                }
            }
        }
        #endregion

        #region Public Methods
        [Inject] 
        public void Constructor(GameManager _gameManager, 
            PhotonService _photonService, 
            SFXManager _sfxManager)
        {
            gameManager = _gameManager;
            photonService = _photonService;
            sfxManager = _sfxManager;
        }

        // Checks if the selected slot is available
        public void CheckAvailableSlot(GameObject go)
        {
            SlotScript _slotController = go.GetComponent<SlotScript>();
            int _column = _slotController.column - 1;

            for (int i = rows - 1; i >= 0; i--)
            {
                if (slot[_column, i].slotState == SlotScript.SlotState.Empty)
                {
                    sfxManager.PlayClip(SFXClips.SELECT_SLOT);
                    photonView.RPC("SetSlotState", PhotonTargets.All, gameManager.playerColor.ToString(), _column, i);
                    photonService.punTurnManager.SendMove(null, true);
                    return;
                }
            }
        }

        [PunRPC] // Set slot state
        public void SetSlotState(string colorStr, int column, int row)
        {
            slot[column, row].SetSlotState(colorStr);
            if (PhotonNetwork.isMasterClient)
            {
                if ( VerticalCheck(colorStr, column, row) || 
                   HorizontalCheck(colorStr, column, row) ||
                   DiagonalRightDown(colorStr, column, row) ||
                   DiagonalRightUp(colorStr, column, row) ||
                   DiagonalLeftUp(colorStr, column, row) ||
                   DiagonalLeftDown(colorStr, column, row) )
                {
                    photonView.RPC("SetWinner", PhotonTargets.All, colorStr);
                }
            }
            //Debug.Log("VerticalCheck: " + VerticalCheck(colorStr, column, row) );
            //Debug.Log("HorizontalCheck: " + HorizontalCheck(colorStr, column, row));
            //Debug.Log("DiagonalRightDown: " + DiagonalRightDown(colorStr, column, row));
            //Debug.Log("DiagonalRightUp: " + DiagonalRightUp(colorStr, column, row));
            //Debug.Log("DiagonalLeftUp: " + DiagonalLeftUp(colorStr, column, row));
            //Debug.Log("DiagonalLeftDown: " + DiagonalLeftDown(colorStr, column, row));
        }

        [PunRPC] 
        public void SetWinner(string winningColor)
        {
            gameManager.gameState = GameManager.GameState.GAME_ENDED;
            gameManager.SetWinner(winningColor);
        }

        [PunRPC] // Resets slots state
        public void ResetRPC()
        {
            gameManager.gameState = GameManager.GameState.GAME_STARTED;
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    slot[i, j].ResetSlotState();
                }
            }
            gameManager.StartTurn();
        }
        #endregion

        #region Private Methods
        // Check for win Vertical
        private bool VerticalCheck(string color, int _column, int _row)
        {
            int winCount = 0;
            SlotScript.SlotState stateToCheck;

            if (color == "Red")
                stateToCheck = SlotScript.SlotState.Red;
            else
                stateToCheck = SlotScript.SlotState.Green;

            for (int j = 0; j < rows; j++)
            {
                if (slot[_column, j].slotState == stateToCheck)
                {
                    winCount++;
                    if (winCount == 4)
                        return true;
                }
            }

            return false;
        }

        // Check for win Horizontal
        private bool HorizontalCheck(string color, int _column, int _row)
        {
            int winCount = 0;
            SlotScript.SlotState stateToCheck;

            if (color == "Red")
                stateToCheck = SlotScript.SlotState.Red;
            else
                stateToCheck = SlotScript.SlotState.Green;

            for (int i = 0; i < columns; i++)
            {
                if (slot[i, _row].slotState == stateToCheck)
                {
                    winCount++;
                    if (winCount == 4)
                        return true;
                }
                else
                    winCount = 0;
            }

            winCount = 0;

            for (int i = _column; i >= 0; i--)
            {
                if (slot[i, _row].slotState == stateToCheck)
                {
                    winCount++;
                    if (winCount == 4)
                        return true;
                }
                else
                    winCount = 0;
            }

            return false;
        }

        // Check for win Diagonal Right Down
        private bool DiagonalRightDown(string color, int column, int row)
        {
            int winCount = 0;
            SlotScript.SlotState stateToCheck;

            if (color == "Red")
                stateToCheck = SlotScript.SlotState.Red;
            else
                stateToCheck = SlotScript.SlotState.Green;

            while (column < this.columns && row < this.rows)
            {
                if (slot[column, row].slotState == stateToCheck)
                {
                    winCount++;
                    row++;
                    column++;
                    if (winCount == 4)
                        return true;
                }
                else
                {
                    winCount = 0;
                    break;
                } 
            }
            return false;
        }

        // Check for win Diagonal Right Up
        private bool DiagonalRightUp(string color, int column, int row)
        {
            int winCount = 0;
            SlotScript.SlotState stateToCheck;

            if (color == "Red")
                stateToCheck = SlotScript.SlotState.Red;
            else
                stateToCheck = SlotScript.SlotState.Green;

            while (column < this.columns && row >= 0)
            {
                if (slot[column, row].slotState == stateToCheck)
                {
                    winCount++;
                    row--;
                    column++;
                    if (winCount == 4)
                        return true;
                }
                else
                {
                    winCount = 0;
                    break;
                }
            }

            return false;
        }

        // Check for win Diagonal Left Up
        private bool DiagonalLeftUp(string color, int column, int row)
        {
            int winCount = 0;
            SlotScript.SlotState stateToCheck;

            if (color == "Red")
                stateToCheck = SlotScript.SlotState.Red;
            else
                stateToCheck = SlotScript.SlotState.Green;

            while (column >= 0 && row >= 0)
            {
                if (slot[column, row].slotState == stateToCheck)
                {
                    winCount++;
                    row--;
                    column--;
                    if (winCount == 4)
                        return true;
                }
                else
                {
                    winCount = 0;
                    break;
                }
            }

            return false;
        }

        // Check for win Diagonal Left Down
        private bool DiagonalLeftDown(string color, int column, int row)
        {
            int winCount = 0;
            SlotScript.SlotState stateToCheck;

            if (color == "Red")
                stateToCheck = SlotScript.SlotState.Red;
            else
                stateToCheck = SlotScript.SlotState.Green;

            while (column >= 0 && row < this.rows)
            {
                if (slot[column, row].slotState == stateToCheck)
                {
                    winCount++;
                    row++;
                    column--;
                    if (winCount == 4)
                        return true;
                }
                else
                {
                    winCount = 0;
                    break;
                }
            }
            return false;
        }
        #endregion
    }
}

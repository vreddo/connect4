﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using TMPro;

namespace VReddo.Connect4
{
    public class GameManager : MonoBehaviour
    {
        #region Private Variables
        private bool localPlayerTurn = true;
        private SlotManager slotManager;
        private PlayerEvents playerEvent;
        private PhotonService photonService;
        private SFXManager sfxManager;
        #endregion

        #region Public Variables
        public LayerMask layerMask; // Layer for on click

        // Used by VR Pointer
        public GameObject m_LeftAnchor;
        public GameObject m_RightAnchor;
        public GameObject m_HeadAnchor;
        // -Used by VR Pointer

        // UI
        public GameObject connectPanel;
        public GameObject connectingTxt;
        public TextMeshProUGUI playerPrompt;
        public TextMeshProUGUI playerColorTxt;
        public Button rematchBtn;
        public Button connectBtn;
        public Button quitBtn;
        // -UI
        public enum GameState
        {
            INIT,
            CONNECTED,
            GAME_STARTED,
            GAME_ENDED
        }
        public GameState gameState;
        public enum PlayerColor
        {
            Red,
            Green
        }
        public PlayerColor playerColor;
        public bool PlayerTurn
        {
            get
            {
                return localPlayerTurn;
            }
            set
            {
                localPlayerTurn = value;
            }
        }
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            this.PlayerTurn = false;
        }

        void Start()
        {
            //playerPrompt.text = "WAITING FOR OTHER PLAYER";
            rematchBtn.gameObject.SetActive(false);
        }

        void Update()
        {
            if (gameState == GameState.INIT ||
                gameState == GameState.GAME_ENDED ||
                !this.PlayerTurn)
                return;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
        
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit, 100f, layerMask))
                {
                    slotManager.CheckAvailableSlot(hit.transform.gameObject);
                }
            }
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Constructor(SlotManager _slotManager, 
            PlayerEvents _playerEvent, 
            PhotonService _photonService,
            SFXManager _sfxManager)
        {
            slotManager = _slotManager;
            playerEvent = _playerEvent;
            photonService = _photonService;
            sfxManager = _sfxManager;
        }

        // UI Connect Button OnClick
        public void OnConnect()
        {
            sfxManager.PlayClip(SFXClips.SELECT_SLOT);
            photonService.OnConnect();
        }

        // Called by Photon Service on game start
        public void StartTurn()
        {
            //quitBtn.gameObject.SetActive(true);

            if (gameState == GameState.GAME_ENDED)
                return;

            if (photonService.GetIsMasterClient)
            {
                photonService.punTurnManager.BeginTurn();
                playerPrompt.text = "YOUR TURN";
                playerColorTxt.text = "YOUR COLOR: RED";
                playerColor = PlayerColor.Red;
                this.PlayerTurn = true;
            }
            else
            {
                playerPrompt.text = "OTHER PLAYER TURN";
                playerColor = PlayerColor.Green;
                playerColorTxt.text = "YOUR COLOR: GREEN";
                this.PlayerTurn = false;
            }
        }

        // Called by pointer vr on trigger press of slots.
        public void OnSelected(GameObject go)
        {
            if (!this.PlayerTurn ||
                gameState == GameState.GAME_ENDED ||
                gameState == GameState.INIT)
                return;

            slotManager.CheckAvailableSlot(go);
        }

        // Sets UI winner state
        public void SetWinner(string data)
        {
            if(photonService.GetIsMasterClient)
                rematchBtn.gameObject.SetActive(true);

            playerPrompt.text = "Winner: " + data;
        }

        // Called on Rematch button Press
        public void OnRestartGame()
        {
            sfxManager.PlayClip(SFXClips.SELECT_SLOT);
            slotManager.photonView.RPC("ResetRPC", PhotonTargets.All);
            rematchBtn.gameObject.SetActive(false);
        }

        // Called on Quit Button Press
        public void OnQuitGame()
        {
            //StartCoroutine(DoBackToLauncher());
            //PhotonNetwork.LeaveRoom(true);
            //PhotonNetwork.Disconnect();
            photonService.OnQuitGame();
        }

        //public override void OnLeftRoom()
        //{
        //    SceneManager.LoadScene("Launcher");
        //}
        #endregion
    }
}

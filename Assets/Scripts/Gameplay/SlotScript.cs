﻿using UnityEngine;
using Zenject;

namespace VReddo.Connect4
{
    [RequireComponent(typeof(DiscSetter))]
    public class SlotScript : MonoBehaviour
    {
        #region Private Variables
        private DiscSetter discSetter;
        private LayerMask layerMask;
        private GameManager gameManager;
        #endregion

        #region Public Variables
        public int column;
        public enum SlotState
        {
            Empty,
            Red,
            Green
        }
        public SlotState slotState;
        #endregion

        #region Monobehaviour Methods
        void Awake()
        {
            discSetter = this.GetComponent<DiscSetter>();
            slotState = SlotState.Empty;
        }
        #endregion

        #region Public Methods
        [Inject]
        public void Construct(GameManager _gameManager)
        {
            gameManager = _gameManager;
        }

        public void ResetSlotState()
        {
            slotState = SlotState.Empty;
            discSetter.ResetDisc();
            layerMask = LayerMask.NameToLayer("DiscSlot");
            this.gameObject.layer = layerMask;
        }

        //[PunRPC]
        public void SetSlotState(string colorStr)
        {
            if (colorStr.Contains("Red"))
                slotState = SlotState.Red;
            else
                slotState = SlotState.Green;

            layerMask = LayerMask.NameToLayer(colorStr);
            this.gameObject.layer = layerMask;
            discSetter.SetDiscColor(colorStr);
            //CheckWin();
        }

        //public void OnClick(string colorStr)
        //{
        //    photonView.RPC("SetSlotState", PhotonTargets.All, colorStr);
        //}

        //[PunRPC]
        //public void SetWinner(string data)
        //{
        //    gameManager.SetWinner(data);
        //}
        #endregion

        //#region Private Methods (Gameplay)
        //public bool CheckWin()
        //{
        //    if (this.slotState == SlotState.Empty)
        //    {
        //        return false;
        //    }

        //    LayerMask _layerMask;
        //    if (slotState == SlotState.Red)
        //        _layerMask = LayerMask.GetMask("Red");
        //    else
        //        _layerMask = LayerMask.GetMask("Green");

        //    // Raycast Upwards
        //    RaycastHit[] hitsVertUp = Physics.RaycastAll(
        //        transform.position,
        //        Vector3.up,
        //        1.0f,
        //        _layerMask);

        //    // Raycast Downwards
        //   RaycastHit[] hitsVertDown = Physics.RaycastAll(
        //        transform.position,
        //        Vector3.down,
        //        1.0f,
        //        _layerMask);

        //    // Raycast Right
        //    RaycastHit[] hitsRight = Physics.RaycastAll(
        //        transform.position,
        //        Vector3.right,
        //        1.0f,
        //        _layerMask);

        //    // Raycast Left
        //    RaycastHit[] hitsLeft = Physics.RaycastAll(
        //        transform.position,
        //        Vector3.left,
        //        1.0f,
        //        _layerMask);

        //    // Raycast Left Dialog Upwards
        //    RaycastHit[] hitsDiaLeftUp = Physics.RaycastAll(
        //        transform.position,
        //        new Vector3(-1, 1),
        //        1.5f,
        //        _layerMask);

        //    // Raycast Left Dialog Downwards
        //    RaycastHit[] hitsDiaLeftDown = Physics.RaycastAll(
        //        transform.position,
        //        new Vector3(-1, -1),
        //        1.5f,
        //        _layerMask);

        //    // Raycast Right Dialog Upwards
        //    RaycastHit[] hitsDiaRightUp = Physics.RaycastAll(
        //        transform.position,
        //        new Vector3(1, 1),
        //        1.5f,
        //        _layerMask);

        //    // Raycast Right Dialog Downwards
        //    RaycastHit[] hitsDiaRightDown = Physics.RaycastAll(
        //        transform.position,
        //        new Vector3(1, -1),
        //        1.5f,
        //        _layerMask);

        //    //Debug.Log(hitsVertUp.Length + " | " + hitsVertDown.Length + " | " + hitsRight.Length + " | " + hitsLeft.Length + " | " + hitsDiaLeftUp.Length + " | " + hitsDiaLeftDown.Length + " | " + hitsDiaRightUp.Length + " | " + hitsDiaRightDown.Length);


        //    if ((hitsVertUp.Length == 3) ||
        //        (hitsVertDown.Length == 3) ||
        //        (hitsRight.Length == 3) ||
        //        (hitsLeft.Length == 3) ||
        //        (hitsDiaLeftUp.Length == 3) ||
        //        (hitsDiaLeftDown.Length == 3) ||
        //        (hitsDiaRightUp.Length == 3) ||
        //        (hitsDiaRightDown.Length == 3))
        //    {
        //        //photonView.RPC("SetWinner", PhotonTargets.All, slotState.ToString());
        //        return true;
        //    }

        //    return false;
        //}
        //#endregion
    }
}

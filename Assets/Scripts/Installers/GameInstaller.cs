using System;
using UnityEngine;
using Zenject;

namespace VReddo.Connect4
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private SlotManager _slotManager;
        [SerializeField] private SFXManager _sfxManager;
        public GameObject photonService;

        public override void InstallBindings()
        {
            Container.Bind<GameManager>().FromInstance(_gameManager);
            Container.Bind<SlotManager>().FromInstance(_slotManager);
            Container.Bind<SFXManager>().FromInstance(_sfxManager);

            Container.BindInterfacesAndSelfTo<PlayerEvents>().AsSingle();
            Container.Bind<PhotonService>().FromComponentInNewPrefab(photonService).AsSingle();
        }
    }
}
﻿Shader "VReddo/Unlit Classroom Shader"
{
	Properties
	{
		_LightsOn ("Lights On Texture", 2D) = "white" {}
		_LightsOff ("Lights Off Texture", 2D) = "white" {}
		_LightLevel ("Light Level", Range (0, 1)) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _LightsOn;
			sampler2D _LightsOff;
			float4 _LightsOn_ST;
			float _LightLevel;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _LightsOn);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed3 onCol = tex2D(_LightsOn, i.uv);
				fixed3 offCol = tex2D(_LightsOff, i.uv);
				return fixed4(lerp(offCol, onCol, _LightLevel).rgb, 1);
			}
			ENDCG
		}
	}
}

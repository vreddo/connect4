How to Play:
To win Connect Four, all you have to do is connect four of your colored checker pieces in a row, much the same as tic tac toe. This can be done horizontally, vertically or diagonally. Each player will place in one checker piece at a time. This will give you a chance to either build your row, or stop your opponent from getting four in a row.

Getting Started:
Run on Unity Editor
- Disable the VR gameobject on the hierarchy.
- EventSystem gameobject on the hierachy should have OVR Input Module deactivated and Standalone Input Module activated.
- Canvas gameobject on the hierachy should have OVR Raycaster deactivated and Graphic Raycaster activated.

Run on VR
- Make sure VR gameobject is enabled.
- EventSystem gameobject on the hierachy should have OVR Input Module activated and Standalone Input Module deactivated.
- Canvas gameobject on the hierachy should have OVR Raycaster activated and Graphic Raycaster deactivated.
- Switch to android platform on the Build Settings.
- On the PlayerSettings->XR Setting, make sure that virtual reality is marked check and Oculus is added to VR SDK.
- On the PlayerSettings->Other Settings, Set Minimum API Level to API Level 21 'Lollipop'.

Notes:
- This game doesn't have a single play so make sure you have two unity editor running or build a windows build and run the executable file with a unity simultaneously running/VR build running.

Built with:
Unity 2018.3.5.f1

Other Tools used:
Zenject
Oculus